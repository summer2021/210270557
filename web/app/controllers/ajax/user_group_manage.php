<?php
	if ($myUser == null || !isSuperUser($myUser)) {
		become403Page();
	}

	if(isset($_GET['group'])){
		$group = DB::escape($_GET['group']);
		$group = htmlspecialchars($group);
		$cond = "group_id = ".$group.";";
	}else{
		$cond = "1";
	}

?>

<div>
	<h2>成员管理</h2>

	<div class="tab-content">
		<!--<div role="tabpanel" class="tab-pane fade in active" id="groupuser">
			<div id="usergroupuserlist"></div>
		</div>

		<div role="tabpanel" class="tab-pane fade" id="groupnuser">
			<div id="usergroupnuserlist"></div>
		</div>-->
		
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div id="usergroupuserlist"></div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div id="usergroupnuserlist"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	function getuserlist(groupid, pageid){
		$.get("/ajax/user-group-user-list?group=" + groupid + "&page=" + pageid, function(data){
			$('#usergroupuserlist').html(data);
		});
	}

	function getnuserlist(groupid, pageid){
		$.get("/ajax/user-group-nuser-list?group=" + groupid + "&page=" + pageid, function(data){
			$('#usergroupnuserlist').html(data);
		});
	}
	getuserlist(<?php echo $group; ?>);
	getnuserlist(<?php echo $group; ?>);
</script>
